#!/usr/bin/env bash

## This script is based on the post 'Trapping signals in Docker containers'
## at https://medium.com/@gchudnov/trapping-signals-in-docker-containers-7a57fdda7d86


## Files' paths
persistent_data='/rpki-data/data.json'
rpki_data='/rpki-validator/data/data.json'


## If some configuration from previous execution was saved, it copies
## 'data.json' file from the volume to the right locations into the validator
## folder.
if [[ -f $persistent_data ]]; then
    cp $persistent_data $rpki_data
    chmod 644 $rpki_data
    echo '> Previous configuration loaded from data volume.'
fi


## SIGTERM handler
handler() {

    ## If data.json exists in the validator folder, persist it on the volume
    ## directory.
    if [[ -f $rpki_data ]]; then
        cp $rpki_data $persistent_data
        echo '> Configuration persisted on data volume.'
    fi

    exit 143; # 128 + 15 -- SIGTERM
}

## Set the handler.
## When the container receives a SIGTERM, it kills using SIGTERM the validator
## then triggers the handler function that will persist the configurations if
## it exists.
trap 'kill -SIGTERM ${!}; handler' SIGTERM


## Start validator daemon and wait for SIGTERM
/rpki-validator/rpki-validator.sh run & wait ${!}

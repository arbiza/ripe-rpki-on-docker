
## This Dockerfile creates a RIPE RPKI Validator image to run on Docker.
##
## Details available at https://bitbucket.org/arbiza/ripe-rpki-docker
## -----------------------------------------------------------------------------


## Use Debian image
FROM debian:jessie

LABEL mantainer="Lucas Arbiza" \
      description="RIPE RPKI Validator on Docker"


## Enable backport repository to install Java 8
RUN echo "deb http://http.debian.net/debian jessie-backports main" | \
    tee --append /etc/apt/sources.list.d/jessie-backports.list > /dev/null

## Update the system and install required packages
RUN apt-get update && apt-get install -y \
    -t jessie-backports openjdk-8-jdk \
    rsync \
    wget


## Get and untar RIPE Validator
RUN wget https://lirportal.ripe.net/certification/content/static/validator/rpki-validator-app-2.24-dist.tar.gz && \
    tar -xzf rpki-validator-app-2.24-dist.tar.gz

RUN mv rpki-validator-app-2.24 rpki-validator && \
    rm rpki-validator-app-2.24-dist.tar.gz

# Files and directories
RUN mkdir /rpki-data
COPY run.bash /
RUN chmod u+x /run.bash


## Expose ports
EXPOSE 8080 8282


# Rather than persisting the entire 'rpki-validator' directory, this image
# persists only data.json file that is where the validator saves the settings
# configured using web interface. Dockerfile doesn't support excluding
# subdirectories from volumes, so it uses signal handling to copy the
# configuration file to the volume directory - See run.bash file.
#
# Persisting the entire 'rpki-validator' directory causes an error when the
# volume needs to be imported (e.g.: GNS3 portable project) because of the name
# length of files the validator creates.
VOLUME ["/rpki-data"]

ENTRYPOINT ["/bin/bash", "/run.bash"]
